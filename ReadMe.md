# Moritz's Binary Format

## General

Moritz's Binary Format (or short MBF) is a steam binary file format, similar to [NBT](https://minecraft.fandom.com/de/wiki/NBT) and/or [JSON](https://de.wikipedia.org/wiki/JavaScript_Object_Notation). It has some advantages compared to those metioned.

## Implementations
 - Kotlin (JVM, Java): [mbf-kotlin](https://gitlab.bixilon.de/bixilon/mbf-kotlin)


## Features
 - Binary (No string bracked stuff like in json)
 - Compression levels (No Compression, [zstd](https://de.wikipedia.org/wiki/Zstandard), [gzip](https://de.wikipedia.org/wiki/Gzip), [Deflate](https://de.wikipedia.org/wiki/Deflate))
 - Update safe (VERY stable format)
 

## Data types

### null
- Type id: `0`

The null data type has no data. It can be used as a value. In a mixed type, this type is `null`.

### Primitive values
 - Boolean (`bool`) (1)
 - Signed byte (`int8`) (2)
 - Signed short (`int16`) (3)
 - Signed int (`int32`) (4)
 - Signed long (`int64`) (5)
 - UUID (`int128`) (6)

All those values are stored in big endian.

### Floating point numbers
 - Float (32 bit) (7)
 - Double (64 bit) (8)

### Special primitive values
 - VarInt (9)
 - VarLong (19)
 
Technically an int, can be in the format, but an int is an int, so you probably can't make a difference between them. It is normally just used in "Variable prefix" mode. More on that in "Length prefix". See [LEB128](https://en.wikipedia.org/wiki/LEB128) for the encoding.

### String
- Type id: `10`
A length prefixed (bytes, not chars) utf8 encoded string.

### (Mixed) collections
There are 2 types of every collections. Normal ones ore mixed. Mixed just means, that the data type for every entry can be different.

#### General format
 - Length prefixed (not in bytes, in entries)
 - If not mixed: Tag type (byte)
 
#### Array (11, 12)
An array is a primitive array, it is technically the same as a list, but in most languages you want to work with arrays.

#### List (13, 14)
A list is a collection of values.

#### Set (15, 16)
A set is an order independant list. Values can not be in there twice.

#### Map (17, 18)
A map is a key -> value map. Every key can just be in there once, but a value can be multiple times. Different (compared to other collections), in normal mode, there are 2 bytes for the data type (key, value). In mixed mode, the key is data type prefixed and the value is also data type prefixed. (If the key is a string), snake case is prefered, `TestData` is bad, `test_data` is good.
 
 
 
## Length prefix
Depends on the "Variable prefix" flag. If the flag is set, a VarInt is used as prefix, otherwise an 32 bit integer.

## General Format
Before any data follows, there is always the header (see below). Before any data there is mostly (except in collections) one byte for the data type.


### Flags
 - Compression (`0b11`) (`00` for no compression, `01` (default) for zstd. `10` for gzip, `11` for deflate)
 - Variable length prefix (`0b1000`)
 - Prefer variable types (`0b10000`)
 
### Header
 - `MBF` (3 bytes; ascii encoded).
 - Format version as a byte (currently `0`)
 - Single byte for flags.
 
If the data is compressed, directly after the flags is a length prefix for the data.

## Examples

The MBF binary data is hex encoded (just in the example down here, not actually)

### Simple text

MBF:
```
4D 42 46: MBF (ascii encoded)

00: Version 0

00: Flags: No compression, fixed length prefix

12: Data type (mixed map)

00 00 00 02: 2 Map entries


 0A 1. Key data type: string
 00 00 00 04 String length: 4 bytes
 6E 61 6D 65: "name"

 0A 1. value data type: string
 00 00 00 06 String length: 6 bytes
 4D 6F 72 69 74 7A: "Moritz"


 0A: 2. Key data type: string
 00 00 00 03: String length: 3 bytes
 61 67 65: "age"

 04: 2. Value data type (int)
 00 00 00 11: Int: 17
```

Json:
```json
{
  "name": "Moritz",
  "age": 17
}
```


### More complex (variable length prefix, lists)

MBF:
```
4D 42 46: MBF (ascii encoded)

00: Version 0

08: Flags: No compression, variable length prefix

12: Data type (mixed map)

04: 4 Map entries


 0A 1. Key data type: string
 04 String length: 4 bytes
 6E 61 6D 65: "name"

 0A 1. value data type: string
 06 String length: 6 bytes
 4D 6F 72 69 74 7A: "Moritz"


 0A: 2. Key data type: string
 03: String length: 3 bytes
 61 67 65: "age"

 04: 2. Value data type (int)
 00 00 00 11: Int: 17


 0A: 3. Key data type: string
 07: String length: 7 bytes
 68 6F 62 62 69 65 73: "hobbies"
 
 0D: 3. Value data type: normal list
 03: 3 entries
 0A: Entry data type: string
 
  08: String length prefix: 8 bytes
  53 6C 65 65 70 69 6E 67: "Sleeping"
  
  06: String length prefix: 6 bytes
  43 6F 64 69 6E 67: "Coding"
  
  15: String length prefix: 21 bytes
  44 65 76 65 6C 6F 70 69 6E 67 20 63 72 61 7A 79 20 73 68 69 74: "Developing crazy shit"
  
 
 0A: 4. Key data type: string
 0B: String length: 11 bytes
 64 65 73 63 72 69 70 74 69 6F 6E: "description"
 
 0A: 4. Value data type: string
 A4 02: String length: 292 bytes
 48 69 20 74 68 65 72 65 2C 20 6D 79 20 6E 61 6D 65 20 69 73 20 4D 6F 72 69 74 7A 2C 20 49 20 64 6F 20 63 72 61 7A 79 20 73 68 69 74 2C 20 6C 69 6B 65 20 64 65 76 65 6C 6F 70 69 6E 67 20 74 68 69 73 20 66 6F 72 6D 61 74 2E 20 49 74 20 77 61 73 20 6A 75 73 74 20 61 6E 20 69 64 65 61 2C 20 49 20 6E 65 65 64 65 64 20 73 6F 6D 65 74 68 69 6E 67 20 74 6F 64 6F 2C 20 62 75 74 20 6E 6F 77 20 79 6F 75 20 73 65 65 20 74 68 69 73 2C 20 73 6F 20 49 20 6D 69 67 68 74 20 62 6C 6F 77 20 79 6F 75 72 20 6D 69 6E 64 2E 20 49 20 61 6D 20 61 6C 73 6F 20 77 6F 72 6B 69 6E 67 20 6F 6E 20 6D 69 6E 65 63 72 61 66 74 20 73 74 75 66 66 2C 20 6C 69 6B 65 20 77 72 69 74 69 6E 67 20 6D 79 20 6F 77 6E 20 63 6C 69 65 6E 74 2E 20 50 72 65 74 74 79 20 75 6E 75 73 75 61 6C 2C 20 62 75 74 20 79 61 68 2E 20 49 20 74 68 69 6E 6B 20 49 20 68 61 76 65 20 74 6F 6F 20 6D 75 63 68 20 74 69 6D 65 20 3A 29: "Hi there, my name is Moritz, I do crazy shit, like developing this format. It was just an idea, I needed something todo, but now you see this, so I might blow your mind. I am also working on minecraft stuff, like writing my own client. Pretty unusual, but yah. I think I have too much time :)"

```

Json:
```json
{
  "name": "Moritz",
  "age": 17,
  "hobbies": [
    "Sleeping",
    "Coding",
    "Developing crazy shit"
  ],
  "description": "Hi there, my name is Moritz, I do crazy shit, like developing this format. It was just an idea, I needed something todo, but now you see this, so I might blow your mind. I am also working on minecraft stuff, like writing my own client. Pretty unusual, but yah. I think I have too much time :)"
  
}
```
